ViewGen("tweets","VueTimerMapReduceTweets")


function ViewGen(store, timesres){

	starttot = new Date().getTime()
	db.createCollection(store+"_viewMapReduce")
	view={"_id" : "ViewInfo",
		"associatedTopic" : "attributDescription",
		"dataProvider" : "grandLyonEvent16959",
		"description" : "this views is a Statistical representation of the attibutes of grandLyonEvent16959",
		"createur" : "Gavin",
		"privacyLevel" : "public"}
	view.releases=[]
	view.code={ "_id" : "code",
		"model-framework" : "MongoMapReduceFinalize"}
	view.code.code = {}
	view.code.code.mapper=map.toString()
	view.code.code.reducer=red.toString()
	view.code.code.finalize=finalizer.toString()
	db[store+"_collection"].find({"_id": "CollectionInfo"})[0].releases.forEach(function(id){
		start=new Date().getTime()
		version= id.split("_")[2]
		db[id].mapReduce(map,red,{'out':store+'_ViewMapReduce_'+version,'finalize':finalizer})
		release = {};
		release._id=store+'_ViewMapReduce_'+version
		release.version=version;
		release.publicationDate=new Date(version);
		release.size=db[store+'_ViewMapReduce_'+version].count();
		release.attributes=[];
		db[store+'_ViewMapReduce_'+version].find().forEach(function(doc){
			release.attributes.push(doc._id)
		})
		view.releases.push(id._id+'View')
		db[store+"_ViewMapReduce"].insert(release);
		end=new Date().getTime()
		timer("process"+id+"Gen",start,end,timesres)
	})
	endtot=new Date().getTime()
	db[store+'_ViewMapReduce'].insert(view);

	timer("FullViewGen",starttot, endtot, timesres)
}


function map(){
	function emitKeyValue(obj,iname){
		for(var k in obj){
			name = iname+"."+k;
			if(k!='_id'){
				if(typeof(obj[k])!="object"){
					if(!isNaN(parseInt(obj[k]))){
						value={};
						value.tab=[parseInt(obj[k])];
						emit(k+".number",value);
					} else if(!isNaN(Date.parse(obj[k]))){
						value={};
						value.tab=[parseInt(obj[k])];
						emit(k+".Date",value);
					} else if(typeof(obj[k])!="function") {
						value={};
						value.tab=[obj[k]];
						emit(k+".String",value);
					};
				} else {
					emitKeyValue(obj[k], name);
				};
			};
		};
	};
	emitKeyValue(this,"grandLyonEvent16959");
}

function red(key, values){
	tabs=[];
	count = 0;
	values.forEach(function(value){
		count = count + value.tab.length;
		tabs= tabs.concat(value.tab);
	});
	res={};
	res["tab"] = tabs;
	return res;
};


function finalizer(key,res){
	result={};
	valueDistribution={};
	tab=res.tab.sort();
	result.data=tab;
	result.median=tab[parseInt(tab.length/2)];
	result.count=tab.length;
	res.tab.forEach(function(value){
		if(valueDistribution[value]==null);
		valueDistribution[value]=0;
		valueDistribution[value]=valueDistribution[value]+1;
	});
	result.valueDistribution=valueDistribution;
	result.type=typeof(result.data[0]);
	if(typeof(result.data[0]=="string")){
		mode={};
		mode.value=null;
		mode.count=0;
		for(key in valueDistribution){
			if(mode.value==null | mode.count<valueDistribution[key]){
				mode.value=key;
				mode.count=valueDistribution[key];
			};
		};
		result.mode=mode;
		return result;
	};
	tot;
	mode={};
	mode.value=null;
	mode.count=0;
	sum=0;
	for(key in valueDistribution){
		if(mode.value==null | mode.count<valueDistribution[key]){
			mode.value=key;
			mode.count=valueDistribution[key];
		};
		sum=sum+valueDistribution[key]*key;
	};
	result.mean=sum/result.count;
	result.mode =mode;
	return result;
};










function timer(value, start, end , timers){

	var start = start
	var end = end
	var time = end - start
	res = {
		"_id":value,"start":start,"end":end,"time":time}
	db[timers].insert(res)
}