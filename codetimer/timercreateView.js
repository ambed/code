function timer(value, start, end , timers){
	var start = start
	var end = end
	var time = end - start
	res = {"_id":value,"start":start,"end":end,"time":time}
	db[timers].insert(res)
}


function createView(url, name, provider, licence, author, description,map, finale, collection, rules){
	db.createCollection(name);
	v={}
	v._id="ViewInfo";
	v.collection=collection
	v.id=url+name;
	v.name=name;
	v.code={ "_id" : "code",
				"model-framework" : "MapFinalizer"}
	v.code.code = {}
	v.code.code.map=map.toString()
	v.code.code.finale=finale.toString()
	v.provider=provider;
	v.licence=licence;
	v.author=author;
	v.description=description;
	v.size=0;
	v.releasesViews=[];
	v.rules=rules.toString()
	db[name].insert(v);
}


function createReleaseView(view, id, date ){
	db.createCollection(view+"_"+id)
	r={}
	r._id=view+"_"+id
	r.url=view+id;
	r.id = id;
	r.publicationDate=date;
	r.size=0;
	r.dataItems=[];
	db[view+"_"+id].insert(r)
	insertReleaseView(view, r)
}

function insertReleaseView(view, releaseView){
	db[view].insert(releaseView);
	db[view].update({'_id':"ViewInfo"},{$push:{"releasesViews":releaseView._id},$inc:{"size":1}})
}



function rule(version){
	return version
}



function mapper(item,id,releaseView, timerres, view){
	
	function emitKeyValue(obj,iname){
		for(var k in obj){
			name = iname+"."+k
			if(k!='_id'){
				if(typeof(obj[k])!="object"){
					if(!isNaN(parseInt(obj[k]))){
						value=parseInt(obj[k]);
						if(db[releaseView].count({'_id':name+".number"})==0){
							db[releaseView].insert({'_id':name+".number",'items':[]})
							db[releaseView].update({'_id':releaseView},{$push:{"dataItems":name+".number"},$inc:{"size":1}})
							db[view].update({'_id':releaseView},{$push:{"dataItems":name+".number"},$inc:{"size":1}})
						}
						db[releaseView].update({'_id':name+".number"},{$push:{'items':value}});
						
					} else if(!isNaN(Date.parse(obj[k]))){
						value=parseInt(obj[k]);
						if(db[releaseView].count({'_id':name+".date"})==0){
							db[releaseView].insert({'_id':name+".date",'items':[]})
							db[releaseView].update({'_id':releaseView},{$push:{"dataItems":name+".date"},$inc:{"size":1}})
							db[view].update({'_id':releaseView},{$push:{"dataItems":name+".date"},$inc:{"size":1}})
						}
						db[releaseView].update({'_id':name+".date"},{$push:{'items':value}});
					} else if(typeof(obj[k])!="function"){
						value=obj[k];
						if(db[releaseView].count({'_id':name+".string"})==0){
							db[releaseView].insert({'_id':name+".string",'items':[]})
							db[releaseView].update({'_id':releaseView},{$push:{"dataItems":name+".string"},$inc:{"size":1}})
							db[view].update({'_id':releaseView},{$push:{"dataItems":name+".date"},$inc:{"size":1}})
						}
						db[releaseView].update({'_id':name+".string"},{$push:{'items':value}});
					};
				} else {
					emitKeyValue(obj[k], name);
				};
			};
		};
	};
	emitKeyValue(item,id);
	
	
}

function finalizer(attrd, releaseView, timerres){
	st=new Date().getTime()
	result={};
	valueDistribution={};
	print(attrd._id)
	tab=attrd.items.sort();
	result.data=tab;
	result.median=tab[parseInt(tab.length/2)];
	result.count=tab.length;
	tab.forEach(function(value){
		if(typeof(value)=="string"){
			value2=""
			value.split(".").forEach(function(word){value2=value2+" "+word})
			value=value2
		}
		if(valueDistribution[value]==null);
			valueDistribution[value]=0;
		valueDistribution[value]=valueDistribution[value]+1;
	});
	result.valueDistribution=valueDistribution;
	result.type=typeof(result.data[0]);
	if(typeof(result.median)=="string"){
		mode={};
		mode.value=null;
		mode.count=0;
		for(key in valueDistribution){
			if(mode.value==null | mode.count<valueDistribution[key]){
				mode.value=key;
				mode.count=valueDistribution[key];
			};
		};
		result.mode=mode;
		db[releaseView].update({'_id':attrd._id},result);
	} else {
		mode={};
		mode.value=null;
		mode.count=0;
		sum=0;
		for(key in valueDistribution){
			if(mode.value==null | mode.count<valueDistribution[key]){
				mode.value=key;
				mode.count=valueDistribution[key];
			};
			sum=sum+valueDistribution[key]*key;
		};
		result.mean=sum/result.count;
		result.mode =mode;
		db[releaseView].update({'_id':attrd._id},result);
	}
	end= new Date().getTime()
	timer("processAtribut"+attrd._id,st,end,timerres)
};


function createViewModel(store,timerres,methode,finale, rules){
	db.createCollection(timerres)
	sttot= new Date().getTime()
	stcol= new Date().getTime()
	createView("localhost",store+"_View", "LIRIS", "public", "Gavin", "this is a Collection",methode,finale, store+"_collection", rules)
	endcol= new Date().getTime()
	timer("CollectionCreationTime",stcol, endcol, timerres)
	collection = db[store+"_collection"].find({"_id":"CollectionInfo"})[0]
	stmap= new Date().getTime()
	releases=collection.releases
	len=releases.length
	for(i=0;i<len;i++){
		rel=releases[i]
		print(rel)
		createReleaseView(store+"_View", rel.split("_")[2], new Date(rel.split("_")[2]*86400000).toString())
		db[rel].find().forEach(function(doc){
			if(doc._id.indexOf("collection")==-1){
				mapper(doc,store,store+"_View_"+rel.split("_")[2], timerres, store+"_View")
			}
		})
	}
	endmap = new Date().getTime()
	timer("mapTime",stmap, endmap, timerres)
	stfin=new Date().getTime()
	db[store+"_View"].find({'_id':'ViewInfo'})[0].releasesViews.forEach(function(releaseView){
		db[releaseView].find().forEach(function(attrd){
			if(attrd._id.indexOf("View")==-1)
				finale(attrd, releaseView, timerres)
		})
		
	})
	endfin=new Date().getTime()
	timer("finTime",stfin, endfin, timerres)
	endtot=new Date().getTime()
	timer("totalTime",sttot, endtot, timerres)
}

createViewModel("grandLyonEvent","VueTimer",mapper,finalizer, rule)
