function createCollection(url, name, provider, licence, author, description){
	db.createCollection(name);
	d={}
	d._id="CollectionInfo";
	d.id=url+name;
	d.name=name;
	d.provider=provider;
	d.licence=licence;
	d.author=author;
	d.description=description;
	d.size=0;
	d.releases=[];
	db[name].insert(d);
}

function createRelease(collection, id, date ){
	db.createCollection(collection.name+"_"+id)
	r={}
	r._id=collection.name+"_"+id
	r.name=collection.name+"_"+id
	r.url=collection.id+id;
	r.id = id;
	r.publicationDate=date;
	r.size=0;
	r.dataItems=[];
	db[collection.name+"_"+id].insert(r)
	insertRelease(r, collection)
}

function insertData(data, release, collection, name){
	release.insert(data)
	release.update({'_id':name},{$push:{"dataItems":data._id},$inc:{"size":1}})
	collection.update({'_id':name},{$push:{"dataItems":data._id},$inc:{"size":1}})
	collection.update({'_id':"CollectionInfo"},{$inc:{"size":1}})
}

function insertRelease(release, collection){
	db[collection.name].insert(release);
	db[collection.name].update({'_id':"CollectionInfo"},{$push:{"releases":release.name}})
}

function createCollectionModel(store,attribut,timerres){
	db.createCollection(timerres)
	sttot= new Date().getTime()
	stcol= new Date().getTime()
	name = store+"_collection"
	createCollection("localhost",name, "LIRIS","public", "Gavin", "this is a Collection")
	endcol= new Date().getTime()
	timer("CollectionCreationTime",stcol, endcol, timerres)
	db[store].find().forEach(function(doc){
		if(doc._id!="ViewInfo"){
			insertst= new Date().getTime()
			attrs=attribut.split(".")
			val=doc
			attrs.forEach(function(attr){
				val=val[attr]
			})
			version=parseInt(Date.parse(val)/(86400000));
			if(db[name+"_"+version].findOne()==null)
			{
				strel= new Date().getTime()
				collection = db[name].find({'_id':'CollectionInfo'})[0]
				createRelease(collection,version, new Date(val).toString())
				endrel= new Date().getTime()
				timer("releaseCreationDate_"+version,strel, endrel, timerres)
			}
			insertData(doc,db[name+"_"+version], db[name], name+"_"+version)
			insertend=  new Date().getTime()
			timer("InsertTime_"+version,insertst, insertend, timerres)
		}
	})
	endtot=new Date().getTime()
	timer("totalTime",sttot, endtot, timerres)
}


 function timer(value, start, end , timers){
	var start = start
	var end = end
	var time = end - start
	res = {"_id":value,"start":start,"end":end,"time":time}
	db[timers].insert(res)
}

createCollectionModel("grandLyonEvent", "properties.creationtime","CollectionTweetsTimer")
